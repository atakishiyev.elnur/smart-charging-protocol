package com.smart.charging;

import com.smart.charging.function.Message;
import com.smart.charging.function.login.LoginRequest;
import com.smart.charging.function.management.EjectPowerBankRequest;
import com.smart.charging.transport.AbstractServerMessageHandler;
import com.smart.charging.transport.server.Server;
import io.netty.channel.ChannelHandlerContext;

import java.util.concurrent.Executors;

public class ServerTest {
    public static void main(String[] args) throws Exception {

        Server server = new Server("localhost", 9100);

        server.setHandler(new AbstractServerMessageHandler() {
            @Override
            public void onMessage(Message message, ChannelHandlerContext ctx) throws Exception {
                System.out.println("Message received from device : " + message.getCmd());
                ctx.writeAndFlush(message);
            }

            @Override
            public void onIncomingConnection(ChannelHandlerContext ctx) throws Exception {
                System.out.println("Incoming connection");
            }

            @Override
            public void onShutdownConnection(ChannelHandlerContext ctx) throws Exception {
                System.out.println("Shutdown connection");
            }
        });

        Executors.newSingleThreadExecutor().submit(() -> {
            while (true) {
                System.in.read();

                EjectPowerBankRequest ejectPowerBankRequest = new EjectPowerBankRequest();
                ejectPowerBankRequest.setSlot((byte)12);
                ejectPowerBankRequest.setVersion((byte)1);
//                ejectPowerBankRequest.setPackLen((short)8);
                ejectPowerBankRequest.setToken(456);


                server.broadcast(ejectPowerBankRequest);
            }
        });
        server.run();

    }
}
