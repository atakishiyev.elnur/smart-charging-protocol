package com.smart.charging;

import com.smart.charging.function.Message;
import com.smart.charging.function.heart.HeartResponse;
import com.smart.charging.function.login.LoginResponse;
import com.smart.charging.function.management.*;
import com.smart.charging.function.query.QueryApnRequest;
import com.smart.charging.function.query.QueryFirmwareVersionRequest;
import com.smart.charging.function.query.QueryICCIDRequest;
import com.smart.charging.function.query.QueryPowerBankInfoRequest;

import java.io.ByteArrayInputStream;

public class ClientMessageFactory {

    public static Message createMessage(byte[] data) throws Exception {
        ByteArrayInputStream is = new ByteArrayInputStream(data);
        short packLen = Message.readShort(is);
        int cmd = Message.readByte(is);
        Message message = null;
        FunctionCodes functionCodes = FunctionCodes.getInstance(cmd);
        switch (functionCodes) {
            case LOGIN:
                message = new LoginResponse();
                break;
            case HEARTBEAT:
                message = new HeartResponse();
                break;
            case QUERY_FIRMWARE:
                message = new QueryFirmwareVersionRequest();
                break;
            case SET_SERVER_ADDRESS:
                message = new SetServerAddressRequest();
                break;
            case QUERY_POWER_BANK_INFROMATION:
                message = new QueryPowerBankInfoRequest();
                break;
            case RENT_POWER_BANK:
                message = new RentPowerBankRequest();
                break;
            case RETURN_POWER_BANK:
                message = new ReturnPowerBankResponse();
                break;
            case REMOTE_RESTART_STATION:
                message = new RemoteRestartStationRequest();
                break;
            case REMOTE_UPDATE_FIRMWARE:
                message = new RemoteUpdateFirmwareRequest();
                break;
            case QUERY_ICCID:
                message = new QueryICCIDRequest();
                break;
            case EJECT_POWER_BANK:
                message = new EjectPowerBankRequest();
                break;
            case SET_APN:
                message = new SetApnRequest();
                break;
            case QUERY_APN:
                message = new QueryApnRequest();
                break;
            case SET_VOICE_LEVEL:
                message = new SetVoiceLevelRequest();
                break;
            case GET_VOICE_LEVEL:
                message = new GetVoiceLevelRequest();
                break;
        }
        if (message != null) {
            message.decodeMessage(is);
        }
        return message;
    }
}
