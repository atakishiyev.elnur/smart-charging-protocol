package com.smart.charging;

import com.smart.charging.function.Message;
import com.smart.charging.function.login.LoginRequest;
import com.smart.charging.transport.AbstractMessageHandler;
import com.smart.charging.transport.client.Client;
import com.sun.management.GarbageCollectionNotificationInfo;
import com.sun.management.GcInfo;
import io.netty.channel.ChannelHandlerContext;

import javax.management.NotificationListener;
import javax.management.openmbean.CompositeDataSupport;
import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryUsage;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ClientTest {
    private static NotificationListener notificationListener = (notification, handback) -> {

        if (notification.getType().equals(GarbageCollectionNotificationInfo.GARBAGE_COLLECTION_NOTIFICATION)) {
            CompositeDataSupport compositeData = (CompositeDataSupport) notification.getUserData();
            GarbageCollectionNotificationInfo gcNotification = GarbageCollectionNotificationInfo.from(compositeData);
            GcInfo gcInfo = gcNotification.getGcInfo();
            Long gcDuration = gcInfo.getDuration();

            Map<String, MemoryUsage> memoryUsageAfterGc = gcInfo.getMemoryUsageAfterGc();
            memoryUsageAfterGc.entrySet().stream().forEach(
                    stringMemoryUsageEntry -> {
                        System.out.println(stringMemoryUsageEntry.getKey() + " : " + stringMemoryUsageEntry.getValue());
                    }
            );

            System.out.println(compositeData);
            System.out.println("GCInfo");
            System.out.println("GcName = " + gcNotification.getGcName());
            System.out.println(";GcAction = " + gcNotification.getGcAction());
            System.out.println(";GcCause = " + gcNotification.getGcCause());
        }
    };

    public static void main(String[] args) throws Exception {
        Client client = new Client("localhost", 9100);

        client.setHandler(new AbstractMessageHandler() {
            @Override
            public void onMessage(Message message, ChannelHandlerContext context) {
                System.out.println("Client : Message received : " + message);
            }
        });


        List<GarbageCollectorMXBean> garbageCollectorMXBeans = ManagementFactory.getGarbageCollectorMXBeans();
        for (GarbageCollectorMXBean garbageCollectorMXBean : garbageCollectorMXBeans) {
            ManagementFactory.getPlatformMBeanServer().
                    addNotificationListener(garbageCollectorMXBean.getObjectName(), notificationListener, null, null);
        }

        System.gc();

        client.start();

        Executors.newScheduledThreadPool(1).schedule(() -> {
            System.out.println("");
        }, 1, TimeUnit.SECONDS);
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setBoxId("Box1");
        loginRequest.setRand(2222);
        loginRequest.setMagic((short) 12);


        client.send(loginRequest);


    }
}
