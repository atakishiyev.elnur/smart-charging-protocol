package com.smart.charging;

import com.smart.charging.function.Message;
import com.smart.charging.function.heart.HeartRequest;
import com.smart.charging.function.login.LoginRequest;
import com.smart.charging.function.management.*;
import com.smart.charging.function.query.QueryApnResponse;
import com.smart.charging.function.query.QueryFirmwareVersionResponse;
import com.smart.charging.function.query.QueryICCIDResponse;
import com.smart.charging.function.query.QueryPowerBankInfoResponse;

import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayInputStream;

public class ServerMessageFactory {
    public static Message createMessage(byte[] data) throws Exception {
        ByteArrayInputStream is = new ByteArrayInputStream(data);
        short packLen = Message.readShort(is);
        int cmd = Message.readByte(is);
        Message message = null;
        FunctionCodes functionCodes = FunctionCodes.getInstance(cmd);
        switch (functionCodes) {
            case LOGIN:
                message = new LoginRequest();
                break;
            case HEARTBEAT:
                message = new HeartRequest();
                break;
            case QUERY_FIRMWARE:
                message = new QueryFirmwareVersionResponse();
                break;
            case SET_SERVER_ADDRESS:
                message = new SetServerAddressResponse();
                break;
            case QUERY_POWER_BANK_INFROMATION:
                message = new QueryPowerBankInfoResponse();
                break;
            case RENT_POWER_BANK:
                message = new RentPowerBankResponse();
                break;
            case RETURN_POWER_BANK:
                message = new ReturnPowerBankRequest();
                break;
            case REMOTE_RESTART_STATION:
                message = new RemoteRestartStationResponse();
                break;
            case REMOTE_UPDATE_FIRMWARE:
                message = new RemoteUpdateFirmwareResponse();
                break;
            case QUERY_ICCID:
                message = new QueryICCIDResponse();
                break;
            case EJECT_POWER_BANK:
                message = new EjectPowerBankResponse();
                break;
            case SET_APN:
                message = new SetApnResponse();
                break;
            case QUERY_APN:
                message = new QueryApnResponse();
                break;
            case SET_VOICE_LEVEL:
                message = new SetVoiceLevelResponse();
                break;
            case GET_VOICE_LEVEL:
                message = new GetVoiceLevelResponse();
                break;
            default:
                throw new Exception("Unexpected message received: " + cmd + " Data = " + DatatypeConverter.printHexBinary(data));
        }

        if (message != null) {
            message.decodeMessage(is);
        }

        return message;
    }
}
