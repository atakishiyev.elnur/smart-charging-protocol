package com.smart.charging;

public enum FunctionCodes {
    LOGIN(0X60),
    HEARTBEAT(0X61),
    QUERY_FIRMWARE(0X62),
    SET_SERVER_ADDRESS(0X63),
    QUERY_POWER_BANK_INFROMATION(0X64),
    RENT_POWER_BANK(0X65),
    RETURN_POWER_BANK(0X66),
    REMOTE_RESTART_STATION(0X67),
    REMOTE_UPDATE_FIRMWARE(0X68),
    QUERY_ICCID(0X69),
    EJECT_POWER_BANK(0X80),
    SET_APN(0X73),
    QUERY_APN(0X74),
    SET_VOICE_LEVEL(0X70),
    GET_VOICE_LEVEL(0X77);


    private int code;

    private FunctionCodes(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public static FunctionCodes getInstance(int value) {
        switch (value) {
            case 0x60:
                return LOGIN;
            case 0x61:
                return HEARTBEAT;
            case 0x62:
                return QUERY_FIRMWARE;
            case 0x63:
                return SET_SERVER_ADDRESS;
            case 0x64:
                return QUERY_POWER_BANK_INFROMATION;
            case 0x65:
                return RENT_POWER_BANK;
            case 0x66:
                return RETURN_POWER_BANK;
            case 0x67:
                return REMOTE_RESTART_STATION;
            case 0x68:
                return REMOTE_UPDATE_FIRMWARE;
            case 0x69:
                return QUERY_ICCID;
            case 0x80:
                return EJECT_POWER_BANK;
            case 0x73:
                return SET_APN;
            case 0x74:
                return QUERY_APN;
            case 0x70:
                return SET_VOICE_LEVEL;
            case 0x77:
                return GET_VOICE_LEVEL;
        }
        return null;
    }
}
