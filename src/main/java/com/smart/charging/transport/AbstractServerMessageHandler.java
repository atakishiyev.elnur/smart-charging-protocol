package com.smart.charging.transport;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelId;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;

public abstract class AbstractServerMessageHandler extends AbstractMessageHandler {
    private DefaultChannelGroup channels = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    public abstract void onIncomingConnection(ChannelHandlerContext ctx) throws Exception;

    public abstract void onShutdownConnection(ChannelHandlerContext ctx) throws Exception;

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        channels.add(ctx.channel());
        onIncomingConnection(ctx);
        super.channelActive(ctx);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        channels.remove(ctx.channel());
        onShutdownConnection(ctx);
        super.channelInactive(ctx);
    }

    public Channel getChannel(ChannelId channelId) {
        return channels.find(channelId);
    }

    public DefaultChannelGroup getChannels() {
        return channels;
    }
}
