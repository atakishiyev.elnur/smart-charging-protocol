package com.smart.charging.transport.client;

import com.smart.charging.ClientMessageFactory;
import com.smart.charging.function.Message;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.DatatypeConverter;
import java.util.List;

@ChannelHandler.Sharable
public class RelinkClientMessageDecoder extends MessageToMessageDecoder<byte[]> {

    private static final Logger logger = LogManager.getLogger(RelinkClientMessageDecoder.class);

    public RelinkClientMessageDecoder() {

    }

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, byte[] bytes, List<Object> list) throws Exception {
        Message message = ClientMessageFactory.createMessage(bytes);
        if (logger.isDebugEnabled()) {
            logger.debug("Incoming: Data = " + DatatypeConverter.printHexBinary(bytes));
        }

        if (message != null) {
            list.add(message);
        }
    }
}
