package com.smart.charging.transport.client;

import com.smart.charging.function.Message;
import com.smart.charging.transport.AbstractMessageHandler;
import com.smart.charging.transport.RelinkMessageEncoder;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.bytes.ByteArrayDecoder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Client {

    private final int port;
    private final String host;
    private Bootstrap bootstrap;
    private AbstractMessageHandler handler;
    private Channel channel;
    private EventLoopGroup workerGroup;
    private static final Logger logger = LogManager.getLogger(Client.class);

    public Client(String host, int port) {
        this.host = host;
        this.port = port;
        workerGroup = new NioEventLoopGroup();
        bootstrap = new Bootstrap();
        bootstrap.group(workerGroup);
        bootstrap.channel(NioSocketChannel.class);
        bootstrap.option(ChannelOption.SO_KEEPALIVE, true);
    }

    public void start() throws InterruptedException {
        bootstrap.handler(new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel socketChannel) throws Exception {
                ChannelPipeline channelPipeline = socketChannel.pipeline()
                        .addLast(new RelinkMessageEncoder())
                        .addLast(new ByteArrayDecoder())
                        .addLast(new RelinkClientMessageDecoder())
                        .addLast(handler);
            }
        });
        logger.info("Trying to connect {}:{}", host, port);
        ChannelFuture sync = bootstrap.connect(host, port).sync();
        channel = sync.channel();
        logger.info("Connected {}:{}", host, port);
    }

    public void stop() throws InterruptedException {
        channel.close();
        workerGroup.shutdownGracefully().sync();
    }

    public void send(Message message) throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Trying to send message: Message = {} Token = {}", message.getCmd(), message.getToken());
        }
        channel.writeAndFlush(message).sync();
    }

    public void setHandler(AbstractMessageHandler handler) {
        this.handler = handler;
    }
}
