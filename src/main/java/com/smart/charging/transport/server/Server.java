package com.smart.charging.transport.server;

import com.smart.charging.function.Message;
import com.smart.charging.transport.AbstractServerMessageHandler;
import com.smart.charging.transport.RelinkMessageEncoder;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.bytes.ByteArrayDecoder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Server {
    private ServerBootstrap serverBootstrap;
    private EventLoopGroup bossGroup;
    private EventLoopGroup workerGroup;
    private String bindAddress;
    private int bindPort;
    private static final Logger logger = LogManager.getLogger(Server.class);

    private AbstractServerMessageHandler handler;
    private Channel channel;

    public Server(String bindAddress, int bindPort) {
        this.bindAddress = bindAddress;
        this.bindPort = bindPort;
    }

    public void run() throws InterruptedException {
        bossGroup = new NioEventLoopGroup();
        workerGroup = new NioEventLoopGroup();
        try {
            serverBootstrap = new ServerBootstrap();
            serverBootstrap.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel socketChannel) throws Exception {
                            ChannelPipeline channelPipeline = socketChannel.pipeline()
                                    .addLast(new RelinkMessageEncoder())
                                    .addLast(new ByteArrayDecoder())
                                    .addLast(new RelinkServerMessageDecoder())
                                    .addLast(handler)
                                    .addLast(new ChannelInboundHandlerAdapter() {

                                    })
                                    .addLast(new ChannelInboundHandlerAdapter() {

                                    });
                        }
                    })
                    .option(ChannelOption.SO_BACKLOG, 128)
                    .childOption(ChannelOption.SO_KEEPALIVE, true);
            ;
            logger.info("Binding to the address: {}:{}", bindAddress, bindPort);
            ChannelFuture f = serverBootstrap.bind(bindAddress, bindPort).sync();
            channel = f.channel();
            logger.info("Successful bound to address: {}:{} ", bindAddress, bindPort);
            f.channel().closeFuture().sync();
        } finally {
            workerGroup.shutdownGracefully().sync();
            bossGroup.shutdownGracefully().sync();
        }
    }

    public void stop() throws InterruptedException {
        channel.closeFuture();
        workerGroup.shutdownGracefully().sync();
        bossGroup.shutdownGracefully().sync();
        logger.info("Server unbound.");
    }

    public void send(ChannelId channelId, Message message) throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Trying to send message to channel {}: Message = {} Token = {}", channelId.asLongText(), message.getCmd(), message.getToken());
        }
        handler.getChannel(channelId).writeAndFlush(message).sync();
    }

    public void broadcast(Message message) throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Trying to broadcast message: Message = {} Token = {}", message.getCmd(), message.getToken());
        }
        handler.getChannels().writeAndFlush(message).sync();
    }


    public void setHandler(AbstractServerMessageHandler handler) {
        this.handler = handler;
    }
}
