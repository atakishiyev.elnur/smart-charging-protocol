package com.smart.charging.transport.server;

import com.smart.charging.ServerMessageFactory;
import com.smart.charging.function.Message;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;

import java.util.List;

@ChannelHandler.Sharable
public class RelinkServerMessageDecoder extends MessageToMessageDecoder<byte[]> {

    public RelinkServerMessageDecoder() {

    }

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, byte[] bytes, List<Object> list) throws Exception {
        Message message = ServerMessageFactory.createMessage(bytes);
        if (message != null) {
            list.add(message);
        }
    }
}
