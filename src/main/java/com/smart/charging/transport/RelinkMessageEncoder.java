package com.smart.charging.transport;

import com.smart.charging.function.Message;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayOutputStream;

@ChannelHandler.Sharable
public class RelinkMessageEncoder extends MessageToByteEncoder<Message> {
    private static final Logger logger = LogManager.getLogger(RelinkMessageEncoder.class);

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, Message message, ByteBuf byteBuf) throws Exception {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        message.encode(os);
        byte[] data = os.toByteArray();
        if (logger.isDebugEnabled()) {
            logger.debug("Outgoing: Data = " + DatatypeConverter.printHexBinary(data));
        }
        byteBuf.writeBytes(Unpooled.copiedBuffer(data));
    }
}
