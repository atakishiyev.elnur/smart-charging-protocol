package com.smart.charging.transport;

import com.smart.charging.function.Message;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

@ChannelHandler.Sharable
public abstract class AbstractMessageHandler extends ChannelInboundHandlerAdapter {

    public abstract void onMessage(Message message, ChannelHandlerContext context) throws Exception;

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        ctx.flush();
        ctx.close();
        super.channelInactive(ctx);
    }

    @Override
    public final void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        Message data = (Message) msg;
        this.onMessage(data, ctx);
    }
}
