package com.smart.charging.function;

import java.io.InputStream;

public interface Decodable {
    public void decode(InputStream is) throws Exception;
}
