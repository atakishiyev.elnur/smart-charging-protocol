package com.smart.charging.function;

import com.smart.charging.FunctionCodes;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public abstract class Message implements Encodable, Decodable {
    protected short packLen;
    protected int version;
    protected int checksum;
    protected int token;

    public void decodeMessage(InputStream is) throws Exception {
        is.reset();
        packLen = readShort(is);
        int command = readByte(is);
        version = readByte(is);
        checksum = readByte(is);
        token = readInt(is);
        decode(is);
    }

    public abstract boolean isResponse();

    public final void encode(OutputStream os) throws Exception {
        byte[] payload = getPayload();
        int payloadLength = 0;
        if (payload != null)
            payloadLength = payload.length;

        packLen = (short) (1 + 1 + 1 + 4 + payloadLength);
        //write packlen
        writeShort(os, packLen);
        //write cmd
        writeByte(os, (byte) getCmd().getCode());
        //write version
        writeByte(os, version);
        //write checksum
        checksum = (byte) calculateChecksum(payload);
        writeByte(os, checksum);
        //write token
        writeInt(os, token);


        if (payload != null && payload.length > 0)
            os.write(payload);
    }


    public abstract FunctionCodes getCmd();

    public static void writeShort(OutputStream os, short value) throws IOException {
        os.write((value >> 8) & 0xFF);
        os.write(value & 0xFF);
    }

    public static void writeByte(OutputStream os, int value) throws IOException {
        os.write(value & 0xFF);
    }

    public static void writeInt(OutputStream os, int value) throws IOException {
        os.write((value >> 24) & 0xFF);
        os.write((value >> 16) & 0xFF);
        os.write((value >> 8) & 0xFF);
        os.write((value) & 0xFF);
    }

    public static void writeString(OutputStream os, String value) throws IOException {
        os.write(value.getBytes());
    }

    public static void writeBytes(OutputStream os, byte[] data) throws IOException {
        os.write(data);
    }

    public static short readShort(InputStream is) throws IOException {
        short s = (byte) (is.read() & 0xFF);
        s = (short) (s << 8);
        s = (short) (s | (is.read() & 0xFF));
        return s;
    }

    public static int readInt(InputStream is) throws IOException {
        int i = is.read() & 0xFF;
        i = (i << 8) | (is.read() & 0xFF);
        i = (i << 8) | (is.read() & 0xFF);
        i = (i << 8) | (is.read() & 0xFF);
        return i;
    }

    public static int readByte(InputStream is) throws IOException {
        return (is.read() & 0xFF);
    }

    public static byte[] readBytes(InputStream is, int length) throws IOException {
        byte[] data = new byte[length];
        is.read(data);
        return data;
    }

    public static String readString(InputStream is, int length) throws IOException {
        byte[] data = new byte[length];
        is.read(data);
        return new String(data);
    }

    private int calculateChecksum(byte[] data) {
        if (data == null) {
            return 0;
        }

        int xor = 0;

        for (int i = 0; i < data.length; i++)
            xor ^= data[i] & 0xFF;

        return xor;
    }


    public abstract byte[] getPayload() throws IOException;

    public short getPackLen() {
        return packLen;
    }

    public void setPackLen(short packLen) {
        this.packLen = packLen;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getChecksum() {
        return checksum;
    }

    public int getToken() {
        return token;
    }

    public void setToken(int token) {
        this.token = token;
    }

}
