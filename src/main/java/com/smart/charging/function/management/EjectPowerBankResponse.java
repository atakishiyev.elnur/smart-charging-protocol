package com.smart.charging.function.management;

import com.smart.charging.FunctionCodes;
import com.smart.charging.function.Decodable;
import com.smart.charging.function.Message;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class EjectPowerBankResponse extends Message implements Decodable {
    private int slot;
    private Result result;
    private byte[] powerBankId;

    public void decode(InputStream is) throws Exception {
        this.slot = readByte(is);
        this.result = Result.getInstance(readByte(is));
        powerBankId = readBytes(is, 8);
    }

    @Override
    public boolean isResponse() {
        return true;
    }

    public FunctionCodes getCmd() {
        return FunctionCodes.EJECT_POWER_BANK;
    }

    public byte[] getPayload() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        writeByte(baos, slot);
        writeByte(baos, (byte) result.getResult());
        writeBytes(baos, powerBankId);
        return baos.toByteArray();
    }

    public int getSlot() {
        return slot;
    }

    public void setSlot(int slot) {
        this.slot = slot;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public byte[] getPowerBankId() {
        return powerBankId;
    }

    public void setPowerBankId(byte[] powerBankId) {
        this.powerBankId = powerBankId;
    }
}
