package com.smart.charging.function.management;

import com.smart.charging.FunctionCodes;
import com.smart.charging.function.Message;

import java.io.IOException;
import java.io.InputStream;

public class RemoteUpdateFirmwareResponse extends Message {
    public FunctionCodes getCmd() {
        return FunctionCodes.REMOTE_UPDATE_FIRMWARE;
    }

    public byte[] getPayload() throws IOException {
        return new byte[0];
    }

    @Override
    public boolean isResponse() {
        return true;
    }

    @Override
    public void decode(InputStream is) throws Exception {

    }
}
