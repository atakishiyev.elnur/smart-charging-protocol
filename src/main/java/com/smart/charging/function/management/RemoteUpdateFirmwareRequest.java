package com.smart.charging.function.management;

import com.smart.charging.FunctionCodes;
import com.smart.charging.function.Decodable;
import com.smart.charging.function.Message;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class RemoteUpdateFirmwareRequest extends Message implements Decodable {

    private String ftpAddress;
    private String ftpPort;
    private String fileName;
    private String username;
    private String password;

    public RemoteUpdateFirmwareResponse createResponse() {
        RemoteUpdateFirmwareResponse response = new RemoteUpdateFirmwareResponse();
        response.setToken(token);
        response.setVersion(version);
        return response;
    }

    public void decode(InputStream is) throws Exception {
        short ftpAddressLength = readShort(is);
        this.ftpAddress = readString(is, ftpAddressLength);

        short ftpPortLen = readShort(is);
        this.ftpPort = readString(is, ftpPortLen);

        short fileNameLen = readShort(is);
        this.fileName = readString(is, fileNameLen);

        short usernameLen = readShort(is);
        username = readString(is, usernameLen);

        short passwordLen = readShort(is);
        password = readString(is, passwordLen);
    }

    public FunctionCodes getCmd() {
        return FunctionCodes.REMOTE_UPDATE_FIRMWARE;
    }

    public byte[] getPayload() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        writeShort(baos, (short) ftpAddress.length());
        writeString(baos, ftpAddress);

        writeShort(baos, (short) ftpPort.length());
        writeString(baos, ftpPort);

        writeShort(baos, (short) fileName.length());
        writeString(baos, fileName);

        writeShort(baos, (short) username.length());
        writeString(baos, username);

        writeShort(baos, (short) password.length());
        writeString(baos, password);
        return baos.toByteArray();
    }

    @Override
    public boolean isResponse() {
        return false;
    }

    public String getFtpAddress() {
        return ftpAddress;
    }

    public void setFtpAddress(String ftpAddress) {
        this.ftpAddress = ftpAddress;
    }

    public String getFtpPort() {
        return ftpPort;
    }

    public void setFtpPort(String ftpPort) {
        this.ftpPort = ftpPort;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
