package com.smart.charging.function.management;

import com.smart.charging.FunctionCodes;
import com.smart.charging.function.Decodable;
import com.smart.charging.function.Message;

import java.io.IOException;
import java.io.InputStream;

public class GetVoiceLevelResponse extends Message implements Decodable {
    private int level;

    public void decode(InputStream is) throws Exception {
        this.level = readByte(is);
    }

    public FunctionCodes getCmd() {
        return FunctionCodes.GET_VOICE_LEVEL;
    }

    public byte[] getPayload() throws IOException {
        return new byte[]{(byte) level};
    }

    @Override
    public boolean isResponse() {
        return true;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
