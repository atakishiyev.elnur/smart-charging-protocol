package com.smart.charging.function.management;

import com.smart.charging.FunctionCodes;
import com.smart.charging.function.Decodable;
import com.smart.charging.function.Message;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class ReturnPowerBankRequest extends Message implements Decodable {
    private int slot;
    private byte[] powerBankId;


    @Override
    public boolean isResponse() {
        return false;
    }

    public ReturnPowerBankResponse createResponse(ReturnPowerBankResponse.Result result) {
        ReturnPowerBankResponse returnPowerBankResponse = new ReturnPowerBankResponse();
        returnPowerBankResponse.setSlot(slot);
        returnPowerBankResponse.setResult(result);
        returnPowerBankResponse.setToken(token);
        returnPowerBankResponse.setVersion(version);
        return returnPowerBankResponse;
    }

    public void decode(InputStream is) throws Exception {
        this.slot = readByte(is);
        this.powerBankId = readBytes(is, 8);
    }

    public FunctionCodes getCmd() {
        return FunctionCodes.RETURN_POWER_BANK;
    }

    public byte[] getPayload() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        writeByte(baos, slot);
        writeBytes(baos, powerBankId);
        return baos.toByteArray();
    }

    public int getSlot() {
        return slot;
    }

    public void setSlot(int slot) {
        this.slot = slot;
    }

    public byte[] getPowerBankId() {
        return powerBankId;
    }

    public void setPowerBankId(byte[] powerBankId) {
        this.powerBankId = powerBankId;
    }
}
