package com.smart.charging.function.management;

import com.smart.charging.FunctionCodes;
import com.smart.charging.function.Decodable;
import com.smart.charging.function.Message;

import java.io.IOException;
import java.io.InputStream;

public class RentPowerBankRequest extends Message implements Decodable {
    private int slot;


    @Override
    public boolean isResponse() {
        return false;
    }

    public RentPowerBankResponse createResponse(byte slot, Result result, byte[] powerBandId) {
        RentPowerBankResponse response = new RentPowerBankResponse();
        response.setSlot(slot);
        response.setResult(result);
        response.setPowerBankId(powerBandId);
        response.setToken(token);
        response.setVersion(version);
        return response;
    }

    public void decode(InputStream is) throws Exception {
        this.slot = readByte(is);
    }

    public FunctionCodes getCmd() {
        return FunctionCodes.RENT_POWER_BANK;
    }

    public byte[] getPayload() throws IOException {
        return new byte[]{(byte) slot};
    }

    public int getSlot() {
        return slot;
    }

    public void setSlot(int slot) {
        this.slot = slot;
    }
}
