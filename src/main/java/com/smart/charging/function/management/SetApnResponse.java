package com.smart.charging.function.management;

import com.smart.charging.FunctionCodes;
import com.smart.charging.function.Decodable;
import com.smart.charging.function.Message;

import java.io.IOException;
import java.io.InputStream;

public class SetApnResponse extends Message implements Decodable {
    private Result result;

    public void decode(InputStream is) throws Exception {
        this.result = Result.getInstance(readByte(is));
    }

    public FunctionCodes getCmd() {
        return FunctionCodes.SET_APN;
    }

    public byte[] getPayload() throws IOException {
        return new byte[]{(byte) result.getResult()};
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    @Override
    public boolean isResponse() {
        return true;
    }
}
