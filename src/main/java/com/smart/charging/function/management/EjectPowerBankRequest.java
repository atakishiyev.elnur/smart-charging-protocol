package com.smart.charging.function.management;

import com.smart.charging.FunctionCodes;
import com.smart.charging.function.Decodable;
import com.smart.charging.function.Message;

import java.io.IOException;
import java.io.InputStream;

public class EjectPowerBankRequest extends Message implements Decodable {
    private int slot;

    public EjectPowerBankResponse createResponse(Result result, byte[] powerBankId) {
        EjectPowerBankResponse response = new EjectPowerBankResponse();
        response.setToken(token);
        response.setVersion(version);
        response.setPowerBankId(powerBankId);
        response.setResult(result);
        response.setSlot(slot);
        return response;
    }

    public void decode(InputStream is) throws Exception {
        this.slot = readByte(is);
    }

    @Override
    public boolean isResponse() {
        return false;
    }

    public FunctionCodes getCmd() {
        return FunctionCodes.EJECT_POWER_BANK;
    }

    public byte[] getPayload() throws IOException {
        return new byte[]{(byte)slot};
    }

    public int getSlot() {
        return slot;
    }

    public void setSlot(int slot) {
        this.slot = slot;
    }
}
