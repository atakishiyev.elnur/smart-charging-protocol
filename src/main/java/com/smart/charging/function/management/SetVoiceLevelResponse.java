package com.smart.charging.function.management;

import com.smart.charging.FunctionCodes;
import com.smart.charging.function.Message;

import java.io.IOException;
import java.io.InputStream;

public class SetVoiceLevelResponse extends Message {
    public FunctionCodes getCmd() {
        return FunctionCodes.SET_VOICE_LEVEL;
    }

    public byte[] getPayload() throws IOException {
        return new byte[0];
    }

    @Override
    public void decode(InputStream is) throws Exception {

    }

    @Override
    public boolean isResponse() {
        return true;
    }
}
