package com.smart.charging.function.management;

import com.smart.charging.FunctionCodes;
import com.smart.charging.function.Message;

import java.io.IOException;
import java.io.InputStream;

public class GetVoiceLevelRequest extends Message {
    public FunctionCodes getCmd() {
        return FunctionCodes.GET_VOICE_LEVEL;
    }

    public byte[] getPayload() throws IOException {
        return new byte[0];
    }

    public GetVoiceLevelResponse createResponse(byte level) {
        GetVoiceLevelResponse response = new GetVoiceLevelResponse();
        response.setToken(token);
        response.setVersion(version);
        response.setLevel(level);
        return response;
    }


    @Override
    public boolean isResponse() {
        return false;
    }

    @Override
    public void decode(InputStream is) throws Exception {

    }
}
