package com.smart.charging.function.management;

public enum Result {
    FAILURE(0),
    SUCCESS(1);


    private int result;

    Result(int result) {
        this.result = result;
    }

    public int getResult() {
        return result;
    }

    public static Result getInstance(int result) {
        switch (result) {
            case 0:
                return FAILURE;
            case 1:
                return SUCCESS;
        }
        return null;
    }
}
