package com.smart.charging.function.management;

import com.smart.charging.FunctionCodes;
import com.smart.charging.function.Decodable;
import com.smart.charging.function.Message;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class ReturnPowerBankResponse extends Message implements Decodable {
    private int slot;
    private Result result;

    public void decode(InputStream is) throws Exception {
        this.slot = readByte(is);
        this.result = Result.getInstance(readByte(is));
    }

    public FunctionCodes getCmd() {
        return FunctionCodes.RETURN_POWER_BANK;
    }

    public byte[] getPayload() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        writeByte(baos, slot);
        writeByte(baos, (byte) result.value);
        return baos.toByteArray();
    }

    public enum Result {
        FAILURE(0),
        SUCCESS(1),
        REPEAT_RETURN(3);
        private int value;

        Result(int value) {
            this.value = value;
        }

        public static Result getInstance(int value) {
            switch (value) {
                case 0:
                    return FAILURE;
                case 1:
                    return SUCCESS;
                case 3:
                    return REPEAT_RETURN;
            }
            return null;
        }

        public int getValue() {
            return value;
        }
    }

    @Override
    public boolean isResponse() {
        return true;
    }

    public int getSlot() {
        return slot;
    }

    public void setSlot(int slot) {
        this.slot = slot;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }
}
