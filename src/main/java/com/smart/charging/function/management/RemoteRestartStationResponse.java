package com.smart.charging.function.management;

public class RemoteRestartStationResponse extends RemoteRestartStationRequest {

    @Override
    public boolean isResponse() {
        return true;
    }
}
