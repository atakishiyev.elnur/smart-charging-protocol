package com.smart.charging.function.management;

import com.smart.charging.FunctionCodes;
import com.smart.charging.function.Decodable;
import com.smart.charging.function.Message;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class SetServerAddressRequest extends Message implements Decodable {
    private String address;
    private String port;
    private int heartbeat;


    @Override
    public boolean isResponse() {
        return false;
    }

    public SetServerAddressResponse createResponse() {
        SetServerAddressResponse response = new SetServerAddressResponse();
        response.setToken(token);
        response.setVersion(version);
        return response;
    }

    public void decode(InputStream is) throws Exception {
        short addressLen = readShort(is);
        this.address = readString(is, addressLen);
        short portLen = readShort(is);
        this.port = readString(is, portLen);
        this.heartbeat = readByte(is);
    }

    public FunctionCodes getCmd() {
        return FunctionCodes.SET_SERVER_ADDRESS;
    }

    public byte[] getPayload() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        short addressLen = (short) address.length();
        writeShort(baos, addressLen);
        writeString(baos, address);
        short portLen = (short) port.length();
        writeShort(baos, portLen);
        writeString(baos, port);
        writeByte(baos, heartbeat);
        return baos.toByteArray();
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public int getHeartbeat() {
        return heartbeat;
    }

    public void setHeartbeat(int heartbeat) {
        this.heartbeat = heartbeat;
    }
}
