package com.smart.charging.function.management;

import com.smart.charging.FunctionCodes;
import com.smart.charging.function.Decodable;
import com.smart.charging.function.Message;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class SetApnRequest extends Message implements Decodable {
    private int index;
    private int valid;
    private String mccMnc;
    private String apn;
    private String username;
    private String password;

    public SetApnResponse createResponse(Result result) {
        SetApnResponse response = new SetApnResponse();
        response.setToken(token);
        response.setVersion(version);
        response.setResult(result);
        return response;
    }

    public void decode(InputStream is) throws Exception {
        this.index = readByte(is);
        this.valid = readByte(is);
        short mccMncLength = readShort(is);
        this.mccMnc = readString(is, mccMncLength);
        short apnLength = readShort(is);
        this.apn = readString(is, apnLength);
        short usernameLen = readShort(is);
        this.username = readString(is, usernameLen);
        short passwordLen = readShort(is);
        this.password = readString(is, passwordLen);
    }


    @Override
    public boolean isResponse() {
        return false;
    }

    public FunctionCodes getCmd() {
        return FunctionCodes.SET_APN;
    }

    public byte[] getPayload() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        writeByte(baos, index);
        writeByte(baos, valid);
        writeShort(baos, (short) mccMnc.length());
        writeString(baos, mccMnc);
        writeShort(baos, (short) apn.length());
        writeString(baos, apn);
        writeShort(baos, (short) username.length());
        writeString(baos, username);
        writeShort(baos, (short) password.length());
        writeString(baos, password);
        return baos.toByteArray();
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getValid() {
        return valid;
    }

    public void setValid(byte valid) {
        this.valid = valid;
    }

    public String getMccMnc() {
        return mccMnc;
    }

    public void setMccMnc(String mccMnc) {
        this.mccMnc = mccMnc;
    }

    public String getApn() {
        return apn;
    }

    public void setApn(String apn) {
        this.apn = apn;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
