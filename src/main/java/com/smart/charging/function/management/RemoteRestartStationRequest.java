package com.smart.charging.function.management;

import com.smart.charging.FunctionCodes;
import com.smart.charging.function.Message;

import java.io.IOException;
import java.io.InputStream;

public class RemoteRestartStationRequest extends Message {
    public FunctionCodes getCmd() {
        return FunctionCodes.REMOTE_RESTART_STATION;
    }

    public RemoteRestartStationResponse createResponse() {
        RemoteRestartStationResponse response = new RemoteRestartStationResponse();
        response.setToken(token);
        response.setVersion(version);
        return response;
    }

    public byte[] getPayload() throws IOException {
        return new byte[0];
    }

    @Override
    public void decode(InputStream is) throws Exception {

    }

    @Override
    public boolean isResponse() {
        return false;
    }
}
