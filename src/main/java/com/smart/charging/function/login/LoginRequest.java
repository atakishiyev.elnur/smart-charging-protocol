package com.smart.charging.function.login;

import com.smart.charging.FunctionCodes;
import com.smart.charging.function.Decodable;
import com.smart.charging.function.Message;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class LoginRequest extends Message implements Decodable {
    private int rand;
    private short magic;
    private String boxId;
    private String reqData;


    @Override
    public boolean isResponse() {
        return false;
    }

    public LoginResponse createResponse(Result result) {
        LoginResponse loginResponse = new LoginResponse();
        loginResponse.setResult(result);
        loginResponse.setVersion(version);
        loginResponse.setToken(token);
        return loginResponse;
    }

    public byte[] getPayload() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        writeInt(baos, rand);
        //write magic
        writeShort(baos, magic);
        //write boxidLen
        writeShort(baos, (short) boxId.length());
        //write boxId
        writeString(baos, boxId);
        //write reqDataLen
        if (reqData != null) {
            writeShort(baos, (short) reqData.length());
            //write reqData
            writeString(baos, reqData);
        }
        return baos.toByteArray();
    }

    public void decode(InputStream is) throws Exception {
        this.rand = readInt(is);
        this.magic = readShort(is);
        short boxIdLen = readShort(is);
        this.boxId = readString(is, boxIdLen);
        short reqDataLen = readShort(is);
        if (reqDataLen == -1 || reqDataLen == 0)
            return;
        this.reqData = readString(is, reqDataLen);
    }

    public FunctionCodes getCmd() {
        return FunctionCodes.LOGIN;
    }


    public int getRand() {
        return rand;
    }

    public void setRand(int rand) {
        this.rand = rand;
    }

    public short getMagic() {
        return magic;
    }

    public void setMagic(short magic) {
        this.magic = magic;
    }


    public String getBoxId() {
        return boxId;
    }

    public void setBoxId(String boxId) {
        this.boxId = boxId;
    }

    public String getReqData() {
        return reqData;
    }

    public void setReqData(String reqData) {
        this.reqData = reqData;
    }
}
