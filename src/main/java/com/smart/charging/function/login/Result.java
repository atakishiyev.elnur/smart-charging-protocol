package com.smart.charging.function.login;

public enum Result {
    FAILURE(0),
    SUCCESS(1),
    FAILURE_CAUSED_BY_WRONG(2);

    private int result;

    Result(int result) {
        this.result = result;
    }

    public int getResult() {
        return result;
    }

    public static Result getInstance(int result) {
        switch (result) {
            case 0:
                return FAILURE;
            case 1:
                return SUCCESS;
            case 2:
                return FAILURE_CAUSED_BY_WRONG;
        }
        return null;
    }
}
