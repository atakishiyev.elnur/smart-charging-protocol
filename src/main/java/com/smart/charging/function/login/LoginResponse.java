package com.smart.charging.function.login;

import com.smart.charging.FunctionCodes;
import com.smart.charging.function.Decodable;
import com.smart.charging.function.Message;

import java.io.IOException;
import java.io.InputStream;

public class LoginResponse extends Message implements Decodable {


    private Result result;

    public byte[] getPayload() throws IOException {
        return new byte[]{(byte) result.getResult()};
    }

    @Override
    public boolean isResponse() {
        return true;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public FunctionCodes getCmd() {
        return FunctionCodes.LOGIN;
    }

    public void decode(InputStream is) throws Exception {
        int b = readByte(is);
        this.result = Result.getInstance(b);
    }
}
