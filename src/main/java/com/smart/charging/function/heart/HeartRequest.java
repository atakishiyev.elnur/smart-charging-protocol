package com.smart.charging.function.heart;

import com.smart.charging.FunctionCodes;
import com.smart.charging.function.Message;

import java.io.IOException;
import java.io.InputStream;

public class HeartRequest extends Message {

    public FunctionCodes getCmd() {
        return FunctionCodes.HEARTBEAT;
    }

    @Override
    public boolean isResponse() {
        return false;
    }

    public HeartResponse createResponse() {
        HeartResponse response = new HeartResponse();
        response.setToken(token);
        response.setVersion(version);
        return response;
    }

    public byte[] getPayload() throws IOException {
        return new byte[0];
    }

    @Override
    public void decode(InputStream is) throws Exception {

    }
}
