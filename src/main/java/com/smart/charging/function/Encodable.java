package com.smart.charging.function;

import java.io.OutputStream;

public interface Encodable {
    public  void encode(OutputStream os) throws Exception;
}
