package com.smart.charging.function.query;

import com.smart.charging.FunctionCodes;
import com.smart.charging.function.Message;

import java.io.IOException;
import java.io.InputStream;

public class QueryICCIDRequest extends Message {
    public FunctionCodes getCmd() {
        return FunctionCodes.QUERY_ICCID;
    }


    public QueryICCIDResponse createResponse(String iccid) {
        QueryICCIDResponse response = new QueryICCIDResponse();
        response.setToken(token);
        response.setVersion(version);
        response.setIccId(iccid);
        return response;
    }

    public byte[] getPayload() throws IOException {
        return new byte[0];
    }


    @Override
    public boolean isResponse() {
        return false;
    }

    @Override
    public void decode(InputStream is) throws Exception {

    }
}
