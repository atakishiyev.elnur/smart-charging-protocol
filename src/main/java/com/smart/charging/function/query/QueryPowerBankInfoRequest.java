package com.smart.charging.function.query;

import com.smart.charging.FunctionCodes;
import com.smart.charging.function.Message;

import java.io.IOException;
import java.io.InputStream;

public class QueryPowerBankInfoRequest extends Message {
    public FunctionCodes getCmd() {
        return FunctionCodes.QUERY_POWER_BANK_INFROMATION;
    }

    public byte[] getPayload() throws IOException {
        return new byte[0];
    }

    /**
     * @param remainNumber
     * @param slot
     * @param powerBandId
     * @param level        0=>1%-20%, 1=>21%-40%, 2=>41%-60%,3=>61%-80%,4=>81%-100%
     * @return
     */
    public QueryPowerBankInfoResponse createResponse(byte remainNumber, byte slot, byte[] powerBandId, byte level) {
        QueryPowerBankInfoResponse response = new QueryPowerBankInfoResponse();
        response.setRemainNum(remainNumber);
        response.setSlot(slot);
        response.setPowerBandId(powerBandId);
        response.setLevel(level);
        response.setToken(token);
        response.setVersion(version);
        return response;
    }


    @Override
    public boolean isResponse() {
        return false;
    }

    @Override
    public void decode(InputStream is) throws Exception {

    }
}
