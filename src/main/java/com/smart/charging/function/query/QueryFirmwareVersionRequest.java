package com.smart.charging.function.query;

import com.smart.charging.FunctionCodes;
import com.smart.charging.function.Message;

import java.io.IOException;
import java.io.InputStream;

public class QueryFirmwareVersionRequest extends Message {
    public FunctionCodes getCmd() {
        return FunctionCodes.QUERY_FIRMWARE;
    }

    public byte[] getPayload() throws IOException {
        return new byte[0];
    }

    public QueryFirmwareVersionResponse createResponse(String softVer) {
        QueryFirmwareVersionResponse response = new QueryFirmwareVersionResponse();
        response.setSoftVer(softVer);
        response.setToken(token);
        response.setVersion(version);
        return response;
    }


    @Override
    public boolean isResponse() {
        return false;
    }

    @Override
    public void decode(InputStream is) throws Exception {

    }
}
