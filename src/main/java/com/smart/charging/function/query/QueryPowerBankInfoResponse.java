package com.smart.charging.function.query;

import com.smart.charging.FunctionCodes;
import com.smart.charging.function.Decodable;
import com.smart.charging.function.Message;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class QueryPowerBankInfoResponse extends Message implements Decodable {
    private int remainNum;
    private int slot;
    private byte[] powerBandId;
    private int level;

    public void decode(InputStream is) throws Exception {
        this.remainNum = readByte(is);
        this.slot = readByte(is);
        this.powerBandId = readBytes(is, 8);
        this.level = readByte(is);
    }

    @Override
    public boolean isResponse() {
        return true;
    }

    public FunctionCodes getCmd() {
        return FunctionCodes.QUERY_POWER_BANK_INFROMATION;
    }

    public byte[] getPayload() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        writeByte(baos, remainNum);
        writeByte(baos, slot);
        writeBytes(baos, powerBandId);
        writeByte(baos, level);
        return baos.toByteArray();
    }

    public int getRemainNum() {
        return remainNum;
    }

    public void setRemainNum(int remainNum) {
        this.remainNum = remainNum;
    }

    public int getSlot() {
        return slot;
    }

    public void setSlot(int slot) {
        this.slot = slot;
    }

    public byte[] getPowerBandId() {
        return powerBandId;
    }

    public void setPowerBandId(byte[] powerBandId) {
        this.powerBandId = powerBandId;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
