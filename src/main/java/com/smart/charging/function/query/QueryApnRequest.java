package com.smart.charging.function.query;

import com.smart.charging.FunctionCodes;
import com.smart.charging.function.Decodable;
import com.smart.charging.function.Message;

import java.io.IOException;
import java.io.InputStream;

public class QueryApnRequest extends Message implements Decodable {
    private int index;

    public void decode(InputStream is) throws Exception {
        this.index = readByte(is);
    }

    public QueryApnResponse createResponse(byte index, byte valid, String mccmnc, String apn, String username,
                                           String password) {
        QueryApnResponse response = new QueryApnResponse();
        response.setToken(token);
        response.setVersion(version);
        response.setIndex(index);
        response.setValid(valid);
        response.setMccMnc(mccmnc);
        response.setApn(apn);
        response.setUsername(username);
        response.setPassword(password);
        return response;
    }

    @Override
    public boolean isResponse() {
        return false;
    }

    public FunctionCodes getCmd() {
        return FunctionCodes.QUERY_APN;
    }

    public byte[] getPayload() throws IOException {
        return new byte[]{(byte) index};
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
