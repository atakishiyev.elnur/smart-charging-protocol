package com.smart.charging.function.query;

import com.smart.charging.FunctionCodes;
import com.smart.charging.function.Decodable;
import com.smart.charging.function.Message;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class QueryICCIDResponse extends Message implements Decodable {
    private String iccId;

    public void decode(InputStream is) throws Exception {
        short iccIdLen = readShort(is);
        this.iccId = readString(is, iccIdLen);
    }

    public FunctionCodes getCmd() {
        return FunctionCodes.QUERY_ICCID;
    }

    public byte[] getPayload() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        writeShort(baos, (short) iccId.length());
        writeString(baos, iccId);
        return baos.toByteArray();
    }

    public String getIccId() {
        return iccId;
    }

    public void setIccId(String iccId) {
        this.iccId = iccId;
    }

    @Override
    public boolean isResponse() {
        return true;
    }
}
