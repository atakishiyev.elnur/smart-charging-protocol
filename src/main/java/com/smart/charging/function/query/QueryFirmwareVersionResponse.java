package com.smart.charging.function.query;

import com.smart.charging.FunctionCodes;
import com.smart.charging.function.Decodable;
import com.smart.charging.function.Message;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class QueryFirmwareVersionResponse extends Message implements Decodable {
    private String softVer;

    public FunctionCodes getCmd() {
        return FunctionCodes.QUERY_FIRMWARE;
    }

    public byte[] getPayload() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        short softVerLen = (short) softVer.getBytes().length;
        writeShort(baos, softVerLen);
        writeString(baos, softVer);
        return baos.toByteArray();
    }

    public void decode(InputStream is) throws Exception {
        short softVerLen = readShort(is);
        this.softVer = readString(is, softVerLen);
    }

    @Override
    public boolean isResponse() {
        return true;
    }

    public String getSoftVer() {
        return softVer;
    }

    public void setSoftVer(String softVer) {
        this.softVer = softVer;
    }
}
