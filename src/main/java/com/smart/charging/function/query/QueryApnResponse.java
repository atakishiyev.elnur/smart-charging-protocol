package com.smart.charging.function.query;

import com.smart.charging.FunctionCodes;
import com.smart.charging.function.Decodable;
import com.smart.charging.function.Message;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class QueryApnResponse extends Message implements Decodable {
    private int index;
    private int valid;
    private String mccMnc;
    private String apn;
    private String username;
    private String password;

    public void decode(InputStream is) throws Exception {
        this.index = readByte(is);
        this.valid = readByte(is);
        short length = readShort(is);
        this.mccMnc = readString(is, length);
        length = readShort(is);
        this.apn = readString(is, length);
        length = readShort(is);
        this.username = readString(is, length);
        length = readShort(is);
        this.password = readString(is, length);
    }

    @Override
    public boolean isResponse() {
        return true;
    }

    public FunctionCodes getCmd() {
        return FunctionCodes.QUERY_APN;
    }

    public byte[] getPayload() throws IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        writeByte(os, index);
        writeByte(os, valid);
        writeShort(os, (short) mccMnc.length());
        writeString(os, mccMnc);
        writeShort(os, (short) apn.length());
        writeString(os, apn);
        writeShort(os, (short) username.length());
        writeString(os, username);
        writeShort(os, (short) password.length());
        writeString(os, password);
        return os.toByteArray();
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getValid() {
        return valid;
    }

    public void setValid(int valid) {
        this.valid = valid;
    }

    public String getMccMnc() {
        return mccMnc;
    }

    public void setMccMnc(String mccMnc) {
        this.mccMnc = mccMnc;
    }

    public String getApn() {
        return apn;
    }

    public void setApn(String apn) {
        this.apn = apn;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
